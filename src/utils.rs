use std::{env, path::PathBuf};

pub fn xdg_runtime_dir() -> PathBuf {
    match env::var("XDG_RUNTIME_DIR") {
        Ok(s) => PathBuf::from(s),
        Err(_) => match env::var("XDG_CACHE_HOME") {
            Ok(s) => PathBuf::from(s),
            Err(_) => PathBuf::from(env::var("HOME").unwrap()).join(".cache"),
        },
    }
}

/// None but for nix's types
pub const NNONE: Option<&str> = None;

pub fn callback_wrapper<F, T, E>(inner: F) -> isize
where
    F: FnOnce() -> Result<T, E>,
    T: std::process::Termination,
    E: std::fmt::Debug,
{
    use std::process::Termination;
    let res = inner();
    match res {
        Ok(_) => {
            res.report();
            0
        }
        Err(_) => {
            res.report();
            1
        }
    }
}
