mod utils;
use eyre::Context;
use libc::SIGCHLD;
use nix::{
    env::clearenv,
    errno::Errno,
    mount::{mount, MsFlags},
    sched::{clone, CloneFlags},
    sys::{
        prctl::set_pdeathsig,
        signal::Signal,
        wait::{waitpid, WaitStatus},
    },
    unistd::{close, pivot_root, Gid, Uid},
};
use std::{ffi::OsString, io::Write};
use std::{
    fs::{self, OpenOptions},
    os::unix::process::CommandExt,
    path::PathBuf,
    process::Command,
};
use tracing::{debug, span};
use tracing::{error, Level};

use crate::utils::{callback_wrapper, xdg_runtime_dir, NNONE};

#[derive(Debug, Clone, clap::Parser)]
/// hpc-shell: use an unpacked guix pack as a shell
struct Args {
    /// Path to the unpacked guix pack. This path should contain ./gnu and ./etc files
    workdir: PathBuf,

    /// Which bash to use
    #[clap(short, long, default_value = "bash")]
    bash: OsString,

    /// Clear all the environment variables
    #[clap(long)]
    clearenv: bool,
}

#[derive(Debug, Clone)]
struct Config {
    uid: Uid,
    gid: Gid,
}

fn main() -> eyre::Result<()> {
    let args = <Args as clap::Parser>::parse();
    let mut child_stack = [0; 4000];

    {
        use tracing_error::ErrorLayer;
        use tracing_subscriber::{fmt, prelude::*, EnvFilter};
        tracing_subscriber::registry()
            .with(fmt::layer().without_time().with_line_number(true))
            .with(EnvFilter::from_default_env())
            .with(ErrorLayer::default())
            .init();
    }

    let config = Config {
        uid: Uid::current(),
        gid: Gid::current(),
    };

    let pipe = unsafe {
        let mut fds = [-1; 2];
        let ret = libc::pipe(fds.as_mut_ptr());
        if ret == -1 {
            return Err(Errno::last()).wrap_err("Failed to create pipe");
        }
        (fds[0], fds[1])
    };
    debug!(?pipe);

    let child = unsafe {
        let args = args.clone();
        let config = config.clone();
        clone(
            Box::new(move || {
                callback_wrapper(|| {
                    set_pdeathsig(Some(Signal::SIGTERM))?;
                    let span = span!(Level::DEBUG, "child");
                    let _enter = span.enter();
                    // close writing pipe
                    close(pipe.1)?;
                    let mut dummy = [0];
                    libc::read(pipe.0, dummy.as_mut_ptr() as _, 1);

                    callback(&args, &config)
                })
            }),
            &mut child_stack,
            CloneFlags::CLONE_NEWUSER | CloneFlags::CLONE_NEWNS,
            Some(SIGCHLD),
        )
    }?;

    debug!("Setting up child!");

    // Close reading pipe
    close(pipe.0)?;

    {
        let mut f = OpenOptions::new()
            .read(true)
            .write(true)
            .open(format!("/proc/{child}/uid_map"))?;
        let msg = format!("0 {} 1", config.uid);
        f.write(msg.as_bytes())
            .wrap_err("Setting uid_map for child process")?;
    }
    {
        let mut f = OpenOptions::new()
            .read(true)
            .write(true)
            .open(format!("/proc/{child}/setgroups"))?;
        f.write("deny".as_bytes())
            .wrap_err("Setting setgroups for child process")?;
    }
    {
        let mut f = OpenOptions::new()
            .read(true)
            .write(true)
            .open(format!("/proc/{child}/gid_map"))?;
        let msg = format!("0 {} 1", config.gid);
        f.write(msg.as_bytes())
            .wrap_err("Setting gid_map for child process")?;
    }

    close(pipe.1)?;

    let r#return = waitpid(child, None)?;
    if let WaitStatus::Exited(_, 0) = r#return {
        debug!(?r#return);
    } else {
        error!(?r#return);
    }

    eprintln!("Goodbye!");

    Ok(())
}

fn callback(args: &Args, _config: &Config) -> eyre::Result<()> {
    let runtime_dir = xdg_runtime_dir().join("hpc-shell").join("runtime");
    debug!(?runtime_dir);
    fs::create_dir_all(&runtime_dir)?;

    let etc_profile = PathBuf::from("/")
        .join(fs::read_link(args.workdir.join("etc")).wrap_err("Reading workdir/etc symlink")?);
    debug!(?etc_profile);

    mount(
        Some("tmpfs"),
        &runtime_dir,
        Some("tmpfs"),
        MsFlags::empty(),
        NNONE,
    )?;

    fs::create_dir_all(runtime_dir.join(".oldroot"))?;

    for entry in fs::read_dir("/")? {
        let entry = entry?.path();

        if entry.is_dir() {
            let new = runtime_dir.join(entry.file_name().unwrap());
            std::fs::create_dir_all(&new)?;
            debug!(?entry, ?new, "bind mounting");

            mount(
                Some(&entry),
                &new,
                NNONE,
                MsFlags::MS_BIND | MsFlags::MS_REC | MsFlags::MS_RDONLY,
                NNONE,
            )?;
        }
    }

    fs::create_dir(runtime_dir.join("gnu"))?;
    mount(
        Some("tmpfs"),
        &runtime_dir.join("gnu"),
        Some("tmpfs"),
        MsFlags::empty(),
        NNONE,
    )?;

    {
        let src = args.workdir.join("gnu");
        let dst = runtime_dir.join("gnu");
        mount(
            Some(&src),
            &dst,
            NNONE,
            MsFlags::MS_BIND | MsFlags::MS_REC | MsFlags::MS_RDONLY,
            NNONE,
        )
        .wrap_err(format!("Mouting {src:?} at {dst:?}"))?;
    }

    pivot_root(&runtime_dir, &runtime_dir)?;

    // unshare(CloneFlags::CLONE_NEWUSER)?;

    // env::set_current_dir(&cwd)?;
    // pivot_root(&runtime_dir, &runtime_dir)?;

    // chroot(&runtime_dir)?;

    // Map back to original user
    // {
    //     let mut f = OpenOptions::new()
    //         .read(true)
    //         .write(true)
    //         .open("/proc/self/uid_map")?;
    //     let msg = format!("{} 0 1", config.uid);
    //     f.write(msg.as_bytes())?;
    // }
    // {
    //     let mut f = OpenOptions::new()
    //         .read(true)
    //         .write(true)
    //         .open("/proc/self/gid_map")?;
    //     let msg = format!("{} 0 1", config.gid);
    //     f.write(msg.as_bytes())?;
    // }

    // env::set_current_dir("/")?;
    eprintln!("Entering hpc-shell!");

    if args.clearenv {
        unsafe { clearenv()? };
    }

    Command::new(&args.bash)
        .arg("--rcfile")
        .arg(etc_profile.join("profile"))
        .exec();

    Ok(())
}
