#! /usr/bin/env -S python3
import os
from pathlib import Path
from functools import reduce
import subprocess
from glob import glob


# newroot_nix = Path(os.environ["WORK"]) / "newroot_nix"
newroot_nix = Path(os.environ["WORK"]) / "nix"

args = []
for p in Path("/").iterdir():
    p = str(p)
    args.append("--dev-bind")
    args.append(p)
    args.append(p)

args = args + [
    # "--setenv", "PATH", "/nix/var/nix/nix/bin" + ":" + os.environ["PATH"],
    "--setenv", "NIX_CONFIG", "extra-experimental-features = nix-command flakes",
    "--setenv", "TMPDIR", os.environ["XDG_RUNTIME_DIR"],
    "--setenv", "BWRAP", "(bwrap) ",
    "--dev-bind", str(newroot_nix), "/nix",
    "--share-net",
]

def run_in_env(cmd):
    subprocess.run([
        "bwrap",
        *args,
        "bash", "--rcfile", "myrc.sh",
        # "-c", cmd
    ], check=True)
  

print(reduce(lambda a, b: f"{a} {b}", args))

if (newroot_nix / "setup_complete").exists():
    print("Bootstraping nix...")
    url = "https://releases.nixos.org/nix/nix-2.9.2/nix-2.9.2-x86_64-linux.tar.xz"
    filename = "nix.tar.xz"

    newroot_nix.mkdir(parents=True, exist_ok=True)

    print("Fetching nix")
    subprocess.run([
        "curl",
        "-L",
        "-o", (newroot_nix / filename),
        url
    ], check=True)

    print("Unpacking nix")
    if False:
        subprocess.run([
            "tar",
            "-x",
            # "-v",
            "--strip-components=1",
            "-C", newroot_nix,
            "-f", (newroot_nix / filename)
        ], check=True)

    for path in (newroot_nix / "store").glob("*"):
        _drv = str(os.path.basename(path))
        _name = _drv[33:]
        if _name.startswith("nix-"):
            nix_real = path
            nix = Path("/nix") / "store" / _drv

    (newroot_nix / "var" / "nix").mkdir(parents=True, exist_ok=True)

    nix_link = (newroot_nix / "var" / "nix" / "nix")
    if nix_link.is_symlink():
        nix_link.unlink()
    nix_link.symlink_to(nix)


    (newroot_nix / "setup_complete").touch()

    run_in_env("nix --version")

    # "nix" , "build", "nixpkgs#path", "-o", "/nix/var/nixpkgs",


exit(0)


os.execv(
    "bwrap",
    args
)
