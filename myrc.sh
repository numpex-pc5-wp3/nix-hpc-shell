if [[ -f ~/.bashrc ]]; then
	source ~/.bashrc
fi

NIX_LOC=/nix/var/nix/nix
source $NIX_LOC/etc/profile.d/nix.sh
export PATH="$NIX_LOC/bin:$PATH"

export NIX_PATH="nixpkgs=/nix/var/nixpkgs"
