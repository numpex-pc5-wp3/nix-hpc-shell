with import <nixpkgs> {
  overlays = [ (final: prev: { hpc-shell = final.callPackage ./package.nix { }; }) ];
};
{
  forShell = hpc-shell.overrideAttrs (prev: { src = null; });

  shell = mkShell {
    packages = [
      rust-analyzer-unwrapped
      rustfmt
      clippy
    ];
    env.RUST_SRC_PATH = "${rustPlatform.rustLibSrc}";
  };

  pkg = pkgsCross.musl64.hpc-shell;
}
